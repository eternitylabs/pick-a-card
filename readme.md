# Google Play Link #
https://play.google.com/store/apps/details?id=com.Sharr.PickACard

# Tobias Fate's Pick a Card #
* A simple game mechanic trainer and simulator for the Champion Twisted "Tobias" Fate in the video game League of Legends
* Written in C# using the Unity engine for Android.
* This was written primarily as an introductory exercise to the Unity engine and Google Play application upload process.
* The spell this simulator is replicating is as follows:
# Pick A Card #
"ACTIVE: Twisted Fate presents a random card which cycles through blue, red and gold for 6 seconds. Reactivating Pick A Card ends the cycle and modifies Twisted Fate's next basic attack to deal magic damage and apply a bonus effect, based on the card he selected."